<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Form Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    
    <form action="/welcome" method="POST">
        @csrf
        <label>First name:</label> 
        <br><br>
        <input type="text" name="nama_awal"> <br><br>
        <label>Last name:</label> 
        <br><br>
        <input type="text" name="nama_akhir"> <br><br>
        <label>Gender:</label> 
        <br><br>
        <input type="radio" name="Gender"> Male<br>
        <input type="radio" name="Gender"> Female<br>
        <input type="radio" name="Gender"> Other<br>
        <br>
        <label>Nationality:</label> 
        <br><br>
        <select name="Nationality:">
            <option value="Indonesia"> Indonesia</option>
            <option value="Singapore"> Singapore</option>
            <option value="Malaysian"> Malaysian</option>
            <option value="Australia"> Australia</option>
        </select>
        <br><br>
        <label>Languange Spoken:</label> 
        <br><br>
        <input type="checkbox"> Bahasa Indonesia<br>
        <input type="checkbox"> English<br>
        <input type="checkbox"> Other<br>
        <br><br>
        <label>Bio:</label><br><br>
        <textarea name="Bio" cols="30" rows="10"></textarea>
        <br>
        <button type="submit"> Sign Up</button>
    </form>
    
    
</body>
</html>